﻿using Shop.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Application.Products.GetProducts
{
    public class GetProducts
    {
        private readonly ApplicationDbContext _context;

        public GetProducts(ApplicationDbContext context)
        {
            _context = context;
        }
        public  IEnumerable<ProductViewModel> Do() =>
           _context.Products.ToList().Select(x => new ProductViewModel  // bax!
            { 
               Name = x.Name,
                Description = x.Description,
                Value = $"$ {x.Value.ToString("N2")}",
            });
        }
    public class ProductViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
    }

}
